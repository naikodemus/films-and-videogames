# FILMS AND VIDEOGAMES

## Criterios

Han de cumplirse todos los posibles. Se descartan los que sólo cumplan uno.

- Ser una obra mainstream.
- Tener protagonista/s femenina/s en exclusiva.
- Identificación neuromotora.
- Identificación narrativa.
- Bajo nivel de abstracción física.
- Mosquear 'incels'.