
inicio="1975/01"
final="2028/01"
max=10

set terminal pdf size 10.0,5.0 enhanced color background rgb 'beige'
set output "films-and-games.pdf"

set title "— PELÍCULAS Y VIDEOJUEGOS —" offset 0,1.75 font ",16"
set border 0 lc rgb 'orange'

set lmargin 5
set rmargin 5
set tmargin 7
set bmargin 4

set termoption enhanced
save_encoding = GPVAL_ENCODING
set encoding utf8

unset key
#set key vert outside l lm 1
set grid back xtics mxtics ls 101

set timefmt "%Y/%m"

set xdata time
set format x "%Y"
set x2data time
set format x2 "%Y"

set tics font ",10" tc 'orange'

#set xzeroaxis
#opción de XTICS para alinear las etiquetas: axis
set xtics rotate by 90 right offset 0,0.25 in scale 0.5,0.5
set xrange [inicio:final]
set x2tics rotate by 90 right offset 0,1.5 in scale 0.5,0.5
set x2range [inicio:final]

set mxtics
set mx2tics

unset ytics
set yrange [(max*-1)-1:max+1]

set style line 1 lw 2 lc rgb 'blue' pt 7 ps 0.5
set style line 2 lw 2 lc rgb 'red' pt 7 ps 0.5

# # #

plot \
     './films.dat' using 1:2 with points ls 1 t 'Películas', \
     './games.dat' using 1:($2*-1) with points ls 2 t 'Videojuegos', \
     './films.dat' using 1:2:3 with labels left offset 1 t '', \
     './games.dat' using 1:($2*-1):3 with labels left offset 1 t ''
